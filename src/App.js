import React from 'react';

import {  BrowserRouter,Route } from "react-router-dom";
import  { IntlProvider } from "react-intl"
import ListStudent from './components/ListStudent';
import Register from './components/Register';
import EditStudent from './components/EditStudent';
import AppHeader from './components/Header';
import Footer from './components/Footer';
import LogoTwit from './assets/Twitter.png';
import LogoGoogle from './assets/google_circle.png';
import LogoFacebook from './assets/Facebook.png';
import PropTypes from 'prop-types'
import message from './constants/message'
import { connect } from 'react-redux';


const styles = theme => ({
  body: {
    padding: 3 * theme.spacing.unit,
    [theme.breakpoints.down('xs')]: {
      padding: 2 * theme.spacing.unit,
    },
  },
});


class App extends React.Component {
 
     render() {
       const { lang } = this.props;
      return (
       
        <BrowserRouter>
         <IntlProvider locale={lang} messages={message[lang]}>
          <div>
            <AppHeader />
              <Route path="/register" component={Register}/>
              <Route path="/" exact component={Register}/>
              <Route path="/edit" component={EditStudent}/>
             <footer className="footer-bar">
              <img src={LogoTwit} className="Image-logo" />&nbsp;&nbsp;&nbsp;
              <img src={LogoGoogle} className="Image-logo" />&nbsp;&nbsp;&nbsp;
              <img src={LogoFacebook} className="Image-logo" />
              </footer>
          </div>
          </IntlProvider>
        </BrowserRouter>
      
      )
   }
}


App.propTypes = {
  lang: PropTypes.string.isRequired,
}

const mapStateToProps = (state) => {
  console.log(state)
  return {
    lang: state.users.lang,

  }
  
}

export default connect(mapStateToProps)(App);

