import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types'
import { add_user ,login_user } from '../actions';
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl'
import '../css/register.css';

class Register extends React.Component {

  static propTypes = {
    text: PropTypes.string,
    placeholder: PropTypes.string,
    mob_no: PropTypes.string,
    stud_name: PropTypes.string,
    id: PropTypes.number,
    editing: PropTypes.bool,
    newUser: PropTypes.bool
  }


  constructor(props) {
    super(props);
    this.state = {
      company_name: '',
      user_name: '',
      phone_number: '',
      authority_name: '',
      email: '',
      c_password: '',
      password: '',
      user_data: [],
      editing: false,
      showPopup: false,
      user_name1: '',
      password1: '',
    }

    this.add = this.add.bind(this);
    this.login = this.login.bind(this);
  }


  add() {
    var data = {
      "company_name": this.state.company_name,
      "user_name": this.state.user_name,
      "phone_number": this.state.phone_number,
      "authority_name": this.state.authority_name,
      "email": this.state.email,
      "password": this.state.password,
      "c_password": this.state.c_password
    }
    if (this.state.phone_number) {

      if (this.state.user_name) {

        if (this.state.password) {

          if (this.state.email) {
            //dispatch the action ADD_USER
            this.props.add_user(data);
            this.setState({
              company_name: '',
              user_name: '',
              phone_number: '',
              authority_name: '',
              email: '',
              c_password: '',
              password: '',
              //  editing: false
            })
            alert("User register successfully.")

            console.log(this.props)
          } else {

            alert("Please enter email")
          }

        } else {

          alert("Please enter password")
        }

      } else {
        console.log(data)
        alert("Please enter username")
      }

    } else {
      alert("Please enter phone no")
    }



  }



  login() {
    var data = {
      "user_name": this.state.user_name1,
      "password": this.state.password1,
    }


    if (this.state.user_name1) {

      if (this.state.password1) {

        console.log(data)
        //dispatch the action ADD_USER
        this.props.login_user(data);
        alert("User Login")
        this.setState({
          user_name1: '',
          password1: '',
          //  editing: false
        })
        // alert("User login successfully.")

        console.log(this.props)

      } else {

        alert("Please enter password")
      }

    } else {
      alert("Please enter username")
    }





  }

  render() {
    return (
      <div>
        <div className="container padding">

          <div className="form-group row">
            <div className="col-md-7 col-sm-7">
              <h3 className="text-muted text-center text-red">
                <FormattedMessage id="regis" defaultMessage="Registration" />
              </h3>
              <form name="addStudentForm" className="form-vertical border-reg">

                <div className="form-group row">
                  <div className="col-sm-9">
                    <input type="text" className="form-control" value={this.state.company_name} onChange={(e) => this.setState({ company_name: e.target.value })} name="company_name" required />
                  </div>
                  <label className="col-sm-3 col-form-label" >  <FormattedMessage id="c_name" defaultMessage="Company Name" /></label>
                </div>

                <div className="form-group row">
                  <div className="col-sm-4">  </div>
                  <div className="col-sm-5">

                    <label className="form-check form-check-inline">
                      <input className="form-check-input" type="radio" name="type" value="Government " />
                      <span className="form-check-label"> <FormattedMessage id="op1" defaultMessage="Government" /> </span>
                    </label>
                    <label className="form-check form-check-inline">
                      <input className="form-check-input" type="radio" name="type" value="Private" />
                      <span className="form-check-label"> <FormattedMessage id="op2" defaultMessage="Private " /></span>
                    </label>
                  </div>
                  <label className="col-sm-3 col-form-label" ><FormattedMessage id="type" defaultMessage="Type" /></label>
                </div>
                <div className="form-group row">
                  <div className="col-sm-9">
                    <input type="file" className="form-control" value={this.state.logo} onChange={(e) => this.setState({ logo: e.target.value })} name="c_password" required />
                  </div>
                  <label className="col-sm-3 col-form-label" ><FormattedMessage id="logo" defaultMessage="Logo" /></label>
                </div>
                <div className="form-group row">
                  <div className="col-sm-9">
                    <input type="text" className="form-control" value={this.state.authority_name} onChange={(e) => this.setState({ authority_name: e.target.value })} name="authority_name" required />
                  </div>
                  <label className="col-sm-3 col-form-label" ><FormattedMessage id="Autho_Name" defaultMessage="Authority Name" /></label>
                </div>
                <div className="form-group row">
                  <div className="col-sm-9">
                    <input type="number" className="form-control" value={this.state.phone_number} onChange={(e) => this.setState({ phone_number: e.target.value })} name="phone_number" required />
                  </div>
                  <label className="col-sm-3 col-form-label" ><FormattedMessage id="phone_no" defaultMessage="Mobile" /></label>
                </div>
                <div className="form-group row">
                  <div className="col-sm-9">
                    <input type="email" className="form-control" value={this.state.email} onChange={(e) => this.setState({ email: e.target.value })} name="email" required />
                  </div>
                  <label className="col-sm-3 col-form-label" ><FormattedMessage id="email" defaultMessage="Email" /></label>
                </div>


                {/* <div className="form-group row">
                  <div className="col-sm-9">
                    <input type="text" className="form-control" value={this.state.categroy} onChange={(e) => this.setState({ categroy: e.target.value })} name="categroy" required />
                  </div>
                  <label  className="col-sm-3 col-form-label" ><FormattedMessage   id="catego" defaultMessage="Categroy" /></label>
                </div> */}
                <div className="form-group row">
                  <div className="col-sm-9">
                    <input type="text" className="form-control" value={this.state.user_name} onChange={(e) => this.setState({ user_name: e.target.value })} name="user_name" required />
                  </div>
                  <label className="col-sm-3 col-form-label" ><FormattedMessage id="usr_name" defaultMessage="User Name" /></label>
                </div>
                <div className="form-group row">
                  <div className="col-sm-9">
                    <input type="password" className="form-control" value={this.state.password} onChange={(e) => this.setState({ password: e.target.value })} name="password" required />
                  </div>
                  <label className="col-sm-3 col-form-label" ><FormattedMessage id="pass" defaultMessage="Password" /></label>
                </div>
                <div className="form-group row">
                  <div className="col-sm-9">
                    <input type="password" className="form-control" value={this.state.c_password} onChange={(e) => this.setState({ c_password: e.target.value })} name="c_password" required />
                  </div>
                  <label className="col-sm-3 col-form-label" ><FormattedMessage id="c_pass" defaultMessage="Confirm Password" /></label>
                </div>

                <div className="form-group row">
                  <div className="col-sm-9">
                    <button type="button" className="btn btn-primary register-button" onClick={() => this.add()}><FormattedMessage id="register" defaultMessage="Register" /></button>
                  </div>
                  <label className="col-sm-3 col-form-label"></label>
                </div>
              </form>
            </div>
            <div className="col-md-5 col-sm-5">
              {/* <p>الخدمات الآلية للإدارة المدرسية موقع مرتبط  <br />بتطبيق المنادي الذكي " على جوالات الايفون والأندرويد إضغط هنا لعرض قديو يشرح خدمات الموقع في أقل من دقيقتين</p> */}
              <p><FormattedMessage id="desc" defaultMessage="Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem" /></p>
              <h3 className="text-muted text-center"><FormattedMessage id="login" defaultMessage="Login" /></h3>
              <form className="border-login">
                <div className="form-group input-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text"> <i className="fa fa-user"></i> </span>
                  </div>
                  <input name="" className="form-control" placeholder="User Name" type="text" value={this.state.user_name1} onChange={(e) => this.setState({ user_name1: e.target.value })} />
                </div>

                <div className="form-group input-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text"> <i className="fa fa-lock"></i> </span>
                  </div>
                  <input className="form-control" placeholder="password" type="password" value={this.state.password1} onChange={(e) => this.setState({ password1: e.target.value })} />
                </div>
                <div className="form-group">
                  <button type="submit" className="btn btn-primary login-button btn-block" onClick={() => this.login()}><FormattedMessage id="login" defaultMessage="Login" /></button>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }

}

const mapStateToProps = (state) => {
  console.log(state)
  return {
    data: state.add_user,

  }

}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    add_user,login_user
  }, dispatch)

}

export default connect(mapStateToProps, mapDispatchToProps)(Register);