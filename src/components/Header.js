import React from 'react';
import {
  AppBar,
  Toolbar,
  Typography,
} from '@material-ui/core';
import {
  Link
} from "react-router-dom";
import '../css/header.css';
import { FormattedMessage   } from 'react-intl'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { setLocale } from '../actions';
import { bindActionCreators } from 'redux';

class AppHeader extends React.Component {

  constructor(props) {
    super(props);}

   render() {
      return (
         <AppBar position="static">
          <Toolbar>
            <Typography variant="title" color="inherit">
           

            </Typography>
            <FormattedMessage   id="smsApp" defaultMessage="Sms App" />

            <ul className="nav navbar-nav wid-80">
              {/* <li><Link to="/register">Register</Link></li>
              <li><Link to="/">List</Link></li> */}
              {/* <li className="float-right"><a role="button">English</a></li>
              <li className="float-right"><a role="button">Arabic </a></li> */}
              <li className="float-right"><Link to="/">info@nenewe.com</Link></li>
              <li className="float-right"><Link to="/">|</Link></li>               
              <li className="float-right"><Link to="/">خدمات الآلية </Link></li>
              
            </ul>
            <a className="btn-lang" role="button" onClick={() => this.props.setLocale('en')}>English</a>
            <a className="btn-lang" role="button" onClick={() => this.props.setLocale('ar')}>Arabic </a>
          </Toolbar>
        </AppBar>
      )
   }
}

AppHeader.propTypes = {
  setLocale: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => {
  return {
    lang: state.locale.lang,

  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    setLocale
  }, dispatch)

}




export default connect(mapStateToProps, {setLocale})(AppHeader);