import React from 'react';
import {
  AppBar,
  Toolbar,
  Typography,
} from '@material-ui/core';
import {
  Link
} from "react-router-dom";
import '../css/footer.css';



class Footer extends React.Component {
   render() {
      return (
         <AppBar position="static">
          <ul className="nav navbar-nav wid-80">
              <li><Link to="/register">Register</Link></li>
              <li><Link to="/">List</Link></li>
              <li className="float-right"><Link to="/">info@nenewe.com</Link></li>
              <li className="float-right"><Link to="/">|</Link></li>               
              <li className="float-right"><Link to="/">خدمات الآلية </Link></li>
            </ul>
        </AppBar>
      )
   }
}

export default Footer;