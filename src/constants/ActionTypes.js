export const ADD_USER = 'ADD_USER'
export const DELETE_USER = 'DELETE_USER'
export const EDIT_USER = 'EDIT_USER'
export const USERS_LIST = 'USERS_LIST'
export const LOCALE_SET = 'LOCALE_SET'
export const LOGIN_USER = 'LOGIN_USER'


