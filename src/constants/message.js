export default {
    en : {

        "smsApp" :"Sms App",
        "regis":"Registration",
        "c_name":"Company Name",
        "type":"Type",
        "email":"Email",
        "phone_no":"Phone Number",
        "Autho_Name":"Authority Name",
        //"catego":"Categroy",
        "usr_name":"User Name",
        "phone_no":"Mobile",
        "desc" :"Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem",
        "login":"Login",
        "register":"Register",
        "op1":"Government ",
        "op2":"Private",
        "pass":"Password",
        "c_pass":"Confirm Password",
        "logo":"Logo"

    },

    ar : {

        "smsApp" :"تطبيق الرسائل القصيرة",
        "regis":"تسجيل",
        "c_name":"اسم المؤسسة",
        "type":"التوع",
        "email":"البريد الإلكتروني",
        "phone_no":"الجوال",
        "Autho_Name":"المسئول عن النظام",
       // "catego":"الفئة",
        "usr_name":"إسم المستخدم",
        "desc":" الخدمات الآلية للإدارة المدرسية موقع مرتبط  بتطبيق المنادي الذكي  على جوالات الايفون والأندرويد إضغط هنا لعرض قديو يشرح خدمات الموقع في أقل من دقيقتين",
        "login":"تسجيل الدخول ",
        "register":"تأكيد كلمة المرور",
        "op1":"حكومي",
        "op2":"خاص",
        "pass":"كلمة المرور ",
        "c_pass":"تأكيد كلمة المرور",
        "logo":"الشعار"


    }
}

