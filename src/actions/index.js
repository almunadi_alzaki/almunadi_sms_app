import * as types from '../constants/ActionTypes';

//const url = 'http://192.168.1.7:2000/user/'
const url = 'http://138.197.158.182:2000/user/'

export function userlist(){
let user_data =  fetch(url).then(function(response) {
                return response.json();
                })
                .then(response => response.data)
                .catch(function(err){
                    return err
                })
	return {
        type:types.USERS_LIST,
        payload:user_data
		
    }
}
export function add_user(data){
    data = JSON.stringify(data);
    let user_data =  fetch(url+'register',{
                        method: 'POST', 
                        body: data,
                        headers:{
                            'Content-Type': 'application/json'
                        }
                    }).then(function(response) {
                        console.log("aaa",response)
                    return response;
                    })
                    .then(response => response.data)
                    .catch(function(err){
                        console.log(err)
                        return err
                    })

                    console.log(user_data,"222")

    return {
        type:types.LOGIN_USER,
		editing:true,
        payload:user_data
       
    }
    
}


export function login_user(data){
    data = JSON.stringify(data);
    let user_data =  fetch(url+'userlogin',{
                        method: 'POST', 
                        body: data,
                        headers:{
                            'Content-Type': 'application/json'
                        }
                    }).then(function(response) {
                        console.log("aaa",response)
                    return response;
                    })
                    .then(response => response.data)
                    .catch(function(err){
                        console.log(err)
                        return err
                    })

                    console.log(user_data,"222")

    return {
        type:types.ADD_USER,
		editing:true,
        payload:user_data
       
    }
    
}

export function edit_user(edit_data){
        let user_data =  fetch(url+'/update/'+edit_data._id,{
                        method: 'PUT', 
                        body: JSON.stringify(edit_data),
                        headers:{
                            'Content-Type': 'application/json'
                        }
                        }).then(function(response) {
                        return response.json();
                        })
                        .then(response => response.data)
                        .catch(function(err){
                            return err
                        })
    return {
        type:types.EDIT_USER,
        payload:user_data.message,
		editing:true,
    }
}

export function delete_user(del_data){
    console.log(del_data);
let user_data =  fetch(url+'/'+del_data,{
                    method: 'DELETE', 
                    headers:{
                        'Content-Type': 'application/json'
                    }
                    }).then(function(response) {
                    return response.json();
                    })
                    .then(response => response.data)
                    .catch(function(err){
                        return err
                    })
    return {
        type:types.DELETE_USER,
        payload:user_data
    }


   
}


export function setLocale(prm){
    
    return {
        type:types.LOCALE_SET,
        payload:prm
    }


   
}







