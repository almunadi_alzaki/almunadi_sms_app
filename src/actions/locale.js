import * as types from '../constants/ActionTypes';

export const localeSet = lang => ({
    type:types.LOCALE_SET,
    lang
})

export const setLocale = lang => (dispatch) =>{
  //  localStorage.alhubLang= lang;
    dispatch(localeSet(lang));
}


