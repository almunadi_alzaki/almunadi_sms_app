export default function (state = {}, action) {

    switch(action.type){
        case 'USERS_LIST':
        return { ...state, users:action.payload}
        case 'LOCALE_SET':
        return { ...state, lang:action.payload}
        case 'ADD_USER':
          return { ...state, add_user:action.payload}
          case 'LOGIN_USER':
          return { ...state, login_user:action.payload}
		 case 'EDIT_USER':
          return  { ...state, edit_user:action.payload}
		 case 'DELETE_USER':
		return { ...state, users:action.payload}
		
        default :
        return state;
    }

}