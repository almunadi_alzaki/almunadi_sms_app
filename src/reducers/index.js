import {combineReducers} from 'redux';
import users from './user_reducers'
import locale from './locale'

const rootReducer = combineReducers({
    users,
    locale
})

export default rootReducer;