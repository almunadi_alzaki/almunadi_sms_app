import * as types from '../constants/ActionTypes';

export default function locale(state = {lang:'ar'}, action) {

    switch(action.type){
      
        case types.LOCALE_SET:
        return { lang : action.lang };
        default :
        return state;
    }

}